# License Compliance for Monorepos

### Overview
This repository demonstrates running [GitLab License Compliance Scanner](https://docs.gitlab.com/ee/user/compliance/license_compliance/) on monorepo projects. Depending on your pipeline requirements, a team may want to run a license check for each commit across all services, or narrow the scope to one service. The focus for this guided exploration include the following points:

- Run a single job to recursively scan each service for license changes

- Run a job per service and scan for only license changes to that service 


## License scan all services 

Licence Compliance Scannier Variable: [LICENSE_FINDER_CLI_OPTS](https://docs.gitlab.com/ee/user/compliance/license_compliance/#available-variables)


> Using the [--recursive](https://github.com/pivotal/LicenseFinder#output-from-project_roots) option means the array will include subdirectories that contain a known package manager. With the exception that Gradle and Maven subprojects will not be included.
```
# This will license scan all services and consolidate into one
include:
  - template: License-Scanning.gitlab-ci.yml
  
license_scanning:
  variables: 
    LICENSE_FINDER_CLI_OPTS: "--prepare --recursive"

```



## License scan each service


`--project-path` will scan a specific project path

[`rules:changes`](https://docs.gitlab.com/ee/ci/yaml/#ruleschanges) will run a job for specific directory changes
```
#Include in root .gitlab-ci.yaml
include:
  - template: License-Scanning.gitlab-ci.yml

# Always ignore main scanning job. Let service pipelines manage their own scanning
license_scanning:
  rules:
    - if: $CI_JOB_ID

#Include in service-ci.yaml file 
license_scanning_serviceA:
  extends: license_scanning
  variables: 
    LICENSE_FINDER_CLI_OPTS: "--prepare --project-path=serviceA/"
  rules:
    - changes:
      - serviceA/**/*
```